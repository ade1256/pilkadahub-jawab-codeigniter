<?php $this->load->view('layout/header');?>
<!-- Morris charts -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      PilkadaHub Jawab

    </h1>
    <ol class="breadcrumb">
      <li class="active"><a href="<?php echo base_url('home') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- BAR CHART -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Pertanyaan terjawab oleh tim <?php echo $this->session->userdata('kubu'); ?></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <p>Pertanyaan : <?php echo $pertanyaan['pertanyaan'] ?></p>
            <p>Jawaban: <?php  $jawaban = $this->M_data->get_jawaban($pertanyaan['_id']);
            echo $jawaban[0]['jawaban']; ?></p>
             <img style="width: 50px;" src="<?php echo $jawaban[0]['img']; ?>" />
            <p>Dijawab oleh : <?php echo $jawaban[0]['jawaboleh'] ?></p>
            <br>
            <br>
            <br>
            <a href="<?php echo base_url('jawaban/edit/'.$pertanyaan['_id'])?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
            <a href="<?php echo base_url('jawaban/hapus/'.$pertanyaan['_id'])?>" class="btn btn-primary red"><i class="fa fa-trash"></i></a>
            
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>

</section>
<!-- /.content -->
</div>
<?php $this->load->view('layout/footer');?>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/morris/morris.min.js"></script>
