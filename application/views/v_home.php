<?php $this->load->view('layout/header');?>
<!-- Morris charts -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      PilkadaHub Jawab
      <small>Selamat datang team sukses <?php echo $this->session->userdata('kubu'); ?> !</small>
    </h1>
    <ol class="breadcrumb">
      <li class="active"><a href="<?php echo base_url('home') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- BAR CHART -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Pertanyaan untuk tim <?php echo $this->session->userdata('kubu'); ?></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
           <table id="datatable" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Email</th>
                <th>Pertanyaan</th>
                <th>Tanggal</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              foreach ($pertanyaan as $key){ 
                if ($key['status']=="1") { ?>
                <tr>
                  <td><?php echo $key['nama'] ?></td>
                  <td><?php echo $key['email'] ?></td>
                  <td><?php echo $key['pertanyaan'] ?></td>
                  <td><?php echo $key['tanggal'] ?></td>
                  <td>
                    <a href="<?php echo base_url('home/jawab/'.$key['_id'])?>" class="btn btn-primary green">Jawab</a> 

                   <!--  <a href="<?php echo base_url('home/edit/'.$key['_id'])?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a> -->
                    <a href="<?php echo base_url('home/hapus/'.$key['_id'])?>" class="btn btn-primary red"><i class="fa fa-trash"></i></a> 
                  </td>
                </tr>
                <?php 
              }
            }
            ?>
            <?php 
            foreach ($pertanyaan_semua as $key_semua){
              if ($key['status']=="1") { ?>
              <tr>
                <td><?php echo $key_semua['nama'] ?></td>
                <td><?php echo $key_semua['email'] ?></td>
                <td><?php echo $key_semua['pertanyaan'] ?></td>
                <td><?php echo $key_semua['tanggal'] ?></td>
                <td>
                  <a href="<?php echo base_url('home/jawab/'.$key_semua['_id'])?>" class="btn btn-primary green">Jawab</a> 

                  <!-- <a href="<?php echo base_url('home/edit/'.$key_semua['_id'])?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a> -->
                  <a href="<?php echo base_url('home/hapus/'.$key_semua['_id'])?>" class="btn btn-primary red"><i class="fa fa-trash"></i></a> 
                </td>
              </tr>
              <?php 
            }
          }
          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>

</section>
<!-- /.content -->
</div>
<?php $this->load->view('layout/footer');?>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/morris/morris.min.js"></script>
<script>
  $(function () {
    $('#datatable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>