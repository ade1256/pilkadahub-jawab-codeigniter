<?php $this->load->view('layout/header');?>
<!-- Morris charts -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      PilkadaHub Jawab

    </h1>
    <ol class="breadcrumb">
      <li class="active"><a href="<?php echo base_url('home') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- BAR CHART -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Jawaban pertanyaan tim <?php echo $this->session->userdata('kubu'); ?></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
           <table id="datatable" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Pertanyaan</th>
                <th>Jawaban</th>
                <th>Gambar</th>
                <th>Dijawab Oleh Tim</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
             <?php 
             foreach ($pertanyaan_terjawab as $key) {
              if ($key['tanyaKe']==$this->session->userdata('kubu')) { ?>  
              <tr>
               <td><?php echo $key['pertanyaan'] ?></td>
               <td>
                <?php 
                $jawaban = $this->M_data->get_jawaban($key['_id']);
                echo $jawaban[0]['jawaban'];
                ?>
              </td>
              <td>
                <img style="width: 50px;" src="<?php echo $jawaban[0]['img']; ?>" />
              </td>
              <td>
                <?php echo $jawaban[0]['jawaboleh']; ?>
              </td>
              <td>
                <a href="<?php echo base_url('jawaban/lihat/'.$key['_id'])?>" class="btn btn-primary green">Lihat</a> 
               <a href="<?php echo base_url('jawaban/edit/'.$key['_id'])?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
               <a href="<?php echo base_url('jawaban/deletejawaban/?idjawaban='.$jawaban[0]['_id'].'&idpertanyaan='.$key['_id'])?>" class="btn btn-primary red"><i class="fa fa-trash"></i></a> 
             </td>
           </tr>
           <?php  }
            if ($key['tanyaKe']=='semua') { ?>  
              <tr>
               <td><?php echo $key['pertanyaan'] ?></td>
               <td>
                <?php 
                $jawaban = $this->M_data->get_jawaban($key['_id']);
                echo $jawaban[0]['jawaban'];
                ?>
              </td>
              <td>
                <img style="width: 50px;" src="<?php echo $jawaban[0]['img']; ?>" />
              </td>
              <td>
                <?php echo $jawaban[0]['jawaboleh']; ?>
              </td>
              <td>
                <a href="<?php echo base_url('jawaban/lihat/'.$key['_id'])?>" class="btn btn-primary green">Lihat</a> 
               <a href="<?php echo base_url('jawaban/edit/'.$key['_id'])?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
               <a href="<?php echo base_url('jawaban/deletejawaban/?idjawaban='.$jawaban[0]['_id'].'&idpertanyaan='.$key['_id'])?>" class="btn btn-primary red"><i class="fa fa-trash"></i></a> 
             </td>
           </tr>
           <?php  }
         }
         ?>  
       </tbody>
     </table>
   </div>
   <!-- /.box-body -->
 </div>
 <!-- /.box -->
</div>
</div>

</section>
<!-- /.content -->
</div>
<?php $this->load->view('layout/footer');?>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/morris/morris.min.js"></script>
<script>
  $(function () {
    $('#datatable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>