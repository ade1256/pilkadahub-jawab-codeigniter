<?php $this->load->view('layout/header');?>
<!-- Morris charts -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">

<div class="content-wrapper">
  <!-- Content Header (Page header) -->


  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- BAR CHART -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $pertanyaan['pertanyaan'] ?></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
           <form method="post" action="<?php echo base_url('home/kirim_jawab') ?>">
            <input type="hidden" name="id" value="<?php echo $pertanyaan['_id'] ?>">
            <input type="hidden" name="status" value="2">
            <input type="hidden" name="jawaboleh" value="<?php echo $this->session->userdata('kubu') ?>">
            <label>Jawaban</label>
             <textarea class="form-control"  placeholder="Jawab" name="jawaban"></textarea>
             <br>
             <label>Link Img (Optional)</label>
             <input type="text" name="img" placeholder="https://...." class="form-control" />
             <br>
             <br>
             <input type="submit" name="submit" class="btn btn-primary green" value="Kirim Jawaban"/>
           </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<?php $this->load->view('layout/footer');?>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/morris/morris.min.js"></script>
