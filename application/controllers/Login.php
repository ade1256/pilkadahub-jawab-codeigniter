<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_data');
	}

	public function index(){
		$data['title'] = 'Login';
		$this->load->view('v_login',$data);
	}

	function cek_login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$data['cek_data'] = $this->M_data->cek_data_login($email,$password);
	}

	function logout(){
		$this->session->sess_destroy('status','email','level');
		redirect('login','refresh');
	}
}
