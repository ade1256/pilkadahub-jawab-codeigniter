<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_data');
		if ($this->session->userdata('status')!='login' || $this->session->userdata('auth')!='pilkadahub-jawab') {
			redirect(base_url('login'));
		}
	}


	public function index(){
		$paslon = rawurlencode($this->session->userdata('kubu'));
		$data['title'] = 'Pertanyaan';
		$data['pertanyaan'] = $this->M_data->get_question_paslon($paslon);
		$data['pertanyaan_semua'] = $this->M_data->get_question();
		$this->load->view('v_home',$data);
	}

	function jawab($id){
		// $paslon = rawurlencode($this->session->userdata('kubu'));
		$data['title'] = 'Jawab';
		$data['pertanyaan'] = $this->M_data->get_question_id($id);
		$this->load->view('v_jawab',$data);
	}

	function kirim_jawab(){
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		$jawaban = $this->input->post('jawaban');
		$img = $this->input->post('img');
		$jawaboleh = $this->input->post('jawaboleh');
		$this->M_data->terjawab($id,$status);
		$this->M_data->kirim_jawaban($id,$jawaban,$img,$jawaboleh);
		redirect(base_url('home'),'refresh');

	}
	function hapus($id){
		$this->M_data->hapus($id);
		redirect(base_url('home'),'refresh');
	}
}
