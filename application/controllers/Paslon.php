<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paslon extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_data');
		if ($this->session->userdata('status')!='login') {
			redirect(base_url('login'));
		}
	}
	public function index(){
		$data['paslon'] = $this->M_data->tampil_data_paslon();
		$data['title'] = 'Paslon';
		$this->load->view('v_paslon',$data);
	}
	function tambah(){
		$data['title'] = 'Tambah Paslon';
		$this->load->view('v_paslon_tambah',$data);
	}

	function lihat($id){
		$data['paslon'] = $this->M_data->lihat_paslon($id);
		$data['title'] = 'Lihat Paslon';
		$this->load->view('v_paslon_lihat',$data);
	}

	function hapus($id){
		$this->M_data->hapus_paslon($id);
	}

	function edit($id){
		$data['paslon'] = $this->M_data->lihat_paslon($id);
		$data['title'] = 'Edit Paslon';
		$this->load->view('v_paslon_edit',$data);
	}

	function update(){
		$id= $this->input->post('id');
		$nama_kepala_daerah= $this->input->post('nama_kepala_daerah');
		$gender_kepala_daerah= $this->input->post('gender_kepala_daerah');
		$tempat_lahir_kepala_daerah= $this->input->post('tempat_lahir_kepala_daerah');
		$tanggal_lahir_kepala_daerah= $this->input->post('tanggal_lahir_kepala_daerah');
		$pekerjaan_kepala_daerah= $this->input->post('pekerjaan_kepala_daerah');
		$nama_wakil_kepala_daerah= $this->input->post('nama_wakil_kepala_daerah');
		$gender_wakil_kepala_daerah= $this->input->post('gender_wakil_kepala_daerah');
		$tempat_lahir_wakil= $this->input->post('tempat_lahir_wakil');
		$tanggal_lahir_wakil= $this->input->post('tanggal_lahir_wakil');
		$pekerjaan_wakil_kepala_daerah= $this->input->post('pekerjaan_wakil_kepala_daerah');
		$jumlah_dukungan= $this->input->post('jumlah_dukungan');
		$partai_pendukung= $this->input->post('partai_pendukung');
		$dana_kampanye= $this->input->post('dana_kampanye');

		$this->M_data->update_paslon($id,$nama_kepala_daerah,$gender_kepala_daerah,$tempat_lahir_kepala_daerah,$tanggal_lahir_kepala_daerah,$pekerjaan_kepala_daerah,$nama_wakil_kepala_daerah,$gender_wakil_kepala_daerah,$tempat_lahir_wakil,$tanggal_lahir_wakil,$pekerjaan_wakil_kepala_daerah,$jumlah_dukungan,$partai_pendukung,$dana_kampanye);
	}
	function kirim(){
		$nama_kepala_daerah= $this->input->post('nama_kepala_daerah');
		$gender_kepala_daerah= $this->input->post('gender_kepala_daerah');
		$tempat_lahir_kepala_daerah= $this->input->post('tempat_lahir_kepala_daerah');
		$tanggal_lahir_kepala_daerah= $this->input->post('tanggal_lahir_kepala_daerah');
		$pekerjaan_kepala_daerah= $this->input->post('pekerjaan_kepala_daerah');
		$nama_wakil_kepala_daerah= $this->input->post('nama_wakil_kepala_daerah');
		$gender_wakil_kepala_daerah= $this->input->post('gender_wakil_kepala_daerah');
		$tempat_lahir_wakil= $this->input->post('tempat_lahir_wakil');
		$tanggal_lahir_wakil= $this->input->post('tanggal_lahir_wakil');
		$pekerjaan_wakil_kepala_daerah= $this->input->post('pekerjaan_wakil_kepala_daerah');
		$jumlah_dukungan= $this->input->post('jumlah_dukungan');
		$partai_pendukung= $this->input->post('partai_pendukung');
		$dana_kampanye= $this->input->post('dana_kampanye');

		$this->M_data->kirim_paslon($nama_kepala_daerah,$gender_kepala_daerah,$tempat_lahir_kepala_daerah,$tanggal_lahir_kepala_daerah,$pekerjaan_kepala_daerah,$nama_wakil_kepala_daerah,$gender_wakil_kepala_daerah,$tempat_lahir_wakil,$tanggal_lahir_wakil,$pekerjaan_wakil_kepala_daerah,$jumlah_dukungan,$partai_pendukung,$dana_kampanye);
	}
}