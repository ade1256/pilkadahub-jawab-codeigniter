<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aduan extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_data');
		if ($this->session->userdata('status')!='login') {
			redirect(base_url('login'));
		}
	}
	public function index(){
		$data['aduan'] = $this->M_data->tampil_data_aduan();
		$data['title'] = 'Aduan';
		$this->load->view('v_aduan',$data);
	}

	function lihat($id){
		$data['aduan'] = $this->M_data->lihat_aduan($id);
		$data['title'] = 'Lihat Aduan';
		$this->load->view('v_aduan_lihat',$data);
	}

	function update(){
		$id= $this->input->post('id');
		$nama= $this->input->post('nama');
		$alamat= $this->input->post('alamat');
		$email= $this->input->post('email');
		$no_hp= $this->input->post('no_hp');
		$photo= $this->input->post('photo');
		$tipe= $this->input->post('tipe');
		$isi= $this->input->post('isi');

		$this->M_data->update_aduan($id,$nama,$alamat,$email,$no_hp,$photo,$tipe,$isi);
	}

	function hapus($id){
		$this->M_data->hapus_aduan($id);
	}

	function edit($id){

		$data['aduan'] = $this->M_data->lihat_aduan($id);
		$data['title'] = 'Edit Aduan';
		$this->load->view('v_aduan_edit',$data);
	}

	function tambah(){
		$data['title'] = 'Tambah Aduan';
		$this->load->view('v_aduan_tambah',$data);
	}

	function kirim(){
		$nama= $this->input->post('nama');
		$alamat= $this->input->post('alamat');
		$email= $this->input->post('email');
		$no_hp= $this->input->post('no_hp');
		$photo_key= $this->input->post('keyImg');
		$photo_name= $this->input->post('photo');
		$tipe= $this->input->post('tipe');
		$isi= $this->input->post('isi');
		$this->M_data->kirim_aduan($nama,$alamat,$email,$no_hp,$photo_key,$photo_name,$tipe,$isi);
	}
}