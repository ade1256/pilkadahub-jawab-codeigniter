<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jawaban extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_data');
		if ($this->session->userdata('status')!='login' || $this->session->userdata('auth')!='pilkadahub-jawab') {
			redirect(base_url('login'));
		}
	}


	public function index(){
		$paslon = rawurlencode($this->session->userdata('kubu'));
		$data['title'] = 'Jawaban';
		$data['pertanyaan'] = $this->M_data->get_question_paslon($paslon);
		$data['pertanyaan_semua'] = $this->M_data->get_question();
		$data['pertanyaan_terjawab'] = $this->M_data->get_pertanyaan_terjawab();
		$this->load->view('v_jawaban',$data);
	}

	function lihat($id){
		$paslon = rawurlencode($this->session->userdata('kubu'));
		$data['title'] = 'Jawaban';
		$data['pertanyaan'] = $this->M_data->get_question_id($id);
		$data['pertanyaan_semua'] = $this->M_data->get_question();
		$data['pertanyaan_terjawab'] = $this->M_data->get_pertanyaan_terjawab();
		$this->load->view('v_jawaban_lihat',$data);
	}

	function deletejawaban(){
		$idpertanyaan = $this->input->get('idpertanyaan');
		$idjawaban = $this->input->get('idjawaban');
		$this->M_data->hapus_jawaban($idjawaban);
		$this->M_data->tidak_terjawab($idpertanyaan);
		redirect(base_url('jawaban'),'refresh');
	}

	function edit($id){
		$data['title'] = 'Jawab';
		$data['pertanyaan'] = $this->M_data->get_question_id($id);
		$data['pertanyaan_semua'] = $this->M_data->get_question();
		$data['pertanyaan_terjawab'] = $this->M_data->get_pertanyaan_terjawab();
		$this->load->view('v_jawaban_edit',$data);
	}

	function update_jawaban(){
		$idpertanyaan = $this->input->post('idpertanyaan');
		$idjawaban = $this->input->post('idjawaban');
		$status = $this->input->post('status');
		$jawaban = $this->input->post('jawaban');
		$img = $this->input->post('img');
		$jawaboleh = $this->input->post('jawaboleh');
		$this->M_data->update_jawaban($idpertanyaan,$idjawaban,$jawaban,$img,$jawaboleh);
		redirect(base_url('jawaban'),'refresh');
	}

}
