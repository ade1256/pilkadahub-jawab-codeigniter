<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_data extends CI_Model{

  function cek_data_login($email,$password){
   $curl = curl_init();

   curl_setopt_array($curl, array(
    CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/auth/",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => '{
      "email": "'.$email.'",
      "pass": "'.$password.'"
    }',
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/json",
      "Postman-Token: b25d5046-eeed-4c66-9753-6051109386a5"
    ),
  ));

   $response = curl_exec($curl);
   $err = curl_error($curl);

   curl_close($curl);
   $normal =  strip_tags($response);
   $array = json_decode($normal, TRUE);

   if ($err) {
    echo "cURL Error #:" . $err;
  } else if ($array['success'] == true) {
    $session_login = array(
      'email' => ''.$email.'',
      'password' => ''.$password.'',
      'kubu' => ''.$array['kubu'].'',
      'status' => 'login',
      'auth' => 'pilkadahub-jawab'
    );
    $this->session->set_userdata( $session_login );
    redirect('Home','refresh');
  }
  else if ($array['success'] == false){
    echo $array['message'];
    redirect('Login','refresh');
  }
}

function get_question_paslon($paslon){
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/question/paslon/".$paslon,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/json",
      "Postman-Token: 89355c97-f1c3-4ed1-bb91-ff5b7f19a218"
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);
  $normal =  strip_tags($response);
  $array = json_decode($normal, TRUE);

  return $array;
}
function get_question_id($id){
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/question/".$id,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/json",
      "Postman-Token: 89355c97-f1c3-4ed1-bb91-ff5b7f19a218"
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);
  $normal =  strip_tags($response);
  $array = json_decode($normal, TRUE);

  return $array;
}
function get_question(){
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/question/paslon/semua",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/json",
      "Postman-Token: 89355c97-f1c3-4ed1-bb91-ff5b7f19a218"
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);
  $normal =  strip_tags($response);
  $array = json_decode($normal, TRUE);

  return $array;
}

// STATUS TERJAWAB
function terjawab($id,$status){
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/question/".$id,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "PUT",
    CURLOPT_POSTFIELDS => '{
      "status": "'.$status.'"
    }',
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/json",
      "Postman-Token: b25d5046-eeed-4c66-9753-6051109386a5"
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
    // echo $response;
    // redirect(base_url('home'),'refresh');
  }
}

function kirim_jawaban($id,$jawaban,$img,$jawaboleh){
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/jawaban/",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => '{
      "idPertanyaan": "'.$id.'",
      "jawaban": "'.$jawaban.'",
      "img": "'.$img.'",
      "jawaboleh": "'.$jawaboleh.'"
    }',
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/json",
      "Postman-Token: b25d5046-eeed-4c66-9753-6051109386a5"
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
      // echo $response;
    // redirect(base_url('home'),'refresh');
  }
}
function update_jawaban($idpertanyaan,$idjawaban,$jawaban,$img,$jawaboleh){
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/jawaban/".$idjawaban,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "PUT",
    CURLOPT_POSTFIELDS => '{
      "idPertanyaan": "'.$idpertanyaan.'",
      "jawaban": "'.$jawaban.'",
      "img": "'.$img.'",
      "jawaboleh": "'.$jawaboleh.'"
    }',
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/json",
      "Postman-Token: b25d5046-eeed-4c66-9753-6051109386a5"
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
      // echo $response;
    // redirect(base_url('home'),'refresh');
  }
}
// HAPUS PERTANYAAN
function hapus($id){
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/question/".$id,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "DELETE",
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/json",
      "Postman-Token: 89355c97-f1c3-4ed1-bb91-ff5b7f19a218"
    ),
  ));
  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
      // echo $response;
    // redirect(base_url('home'),'refresh');
  }

}
function hapus_jawaban($idjawaban){
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/jawaban/".$idjawaban,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "DELETE",
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/json",
      "Postman-Token: 89355c97-f1c3-4ed1-bb91-ff5b7f19a218"
    ),
  ));
  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
      // echo $response;
    // redirect(base_url('home'),'refresh');
  }
  
}
function tidak_terjawab($idpertanyaan){
   $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/question/".$idpertanyaan,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "PUT",
    CURLOPT_POSTFIELDS => '{
      "status": "1"
    }',
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/json",
      "Postman-Token: b25d5046-eeed-4c66-9753-6051109386a5"
    ),
  ));
  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
      // echo $response;
    // redirect(base_url('home'),'refresh');
  }
}

function get_pertanyaan_terjawab(){
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/question/status/2",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/json",
      "Postman-Token: 89355c97-f1c3-4ed1-bb91-ff5b7f19a218"
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);
  $normal =  strip_tags($response);
  $array = json_decode($normal, TRUE);

  return $array;
}
function get_jawaban($id){
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://pilkada-hub.herokuapp.com/api/jawaban/pertanyaan/".$id,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "Cache-Control: no-cache",
      "Content-Type: application/json",
      "Postman-Token: 89355c97-f1c3-4ed1-bb91-ff5b7f19a218"
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);
  $normal =  strip_tags($response);
  $array = json_decode($normal, TRUE);

  return $array;
}

}



